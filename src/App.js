import React, { Component } from 'react';
import header from './b1.jpg';
import b1 from './b3.jpg';
import b2 from './b4.jpg';
import b3 from './b5.jpeg';
import b6 from './b6.jpg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className='jones'>
          <span className='header-title'>
            <h1>Trouwen in het buitenland</h1>
            <img src={b6} className='header-image'></img>
          </span>

          <div className='intro text'>
            <span className='firstletter'>T</span>rouwen in het buitenland spreekt tot de verbeelding maar daar bleef het tot enkele jaren geleden meestal ook bij. De dag van vandaag zien we steeds meer koppels die de stap zetten en hun jawoord geven onder de broeierige Toscaanse zon of tussen de wijnranken in de Provence. Maar waar moet je nu precies op letten bij het plannen van een buitenlands huwelijk? Wij helpen je op weg.
          </div>

          <div className='text'>
            <div className='subtitle'>Yes, I do</div>
            Om officieel man en vrouw te worden moet je eerst trouwen voor het stadhuis. Voor sommigen kan het een administratieve bijkomstigheid zijn maar het is belangrijk dat je dit papiertje in handen hebt. Bij een huwelijk in het buitenland kan het nog een extra voordeel bieden. Zo kunnen gasten die niet op je huwelijk geraken, er wel bij zijn in het stadhuis, waardoor ze toch deel uitmaken van je grote dag.
          </div>

          <div className='text'>
            <div className='subtitle'>Droomlocatie</div>
            Dé vraag waar het allemaal mee begint is waar jullie nu precies willen trouwen. Kies je voor de bestemming waar jullie de eerste keer op reis zijn geweest met z’n tweetjes? Is een van jullie afkomstig van een ander land en wil hij of zij daar trouwen? Of keren jullie terug naar de speciale plek waar hij je ten huwelijk heeft gevraagd? Het kan allemaal. Vanaf deze keuze gemaakt is kan er begonnen worden met het echte werk. Je zal onder meer een locatie moeten zoeken, vliegtuigtickets boeken en in sommige gevallen een visum moeten aanvragen.
          </div>

          <img src={header} className='header-image'></img>

          <div className='text'>
            <div className='subtitle'>Genodigden</div>
            Bij een huwelijk in het buitenland zal niet iedereen die je uitnodigt ook effectief aanwezig zijn. Voor sommigen is de locatie misschien te ver, anderen kunnen zich niet vrijmaken of het is gewoonweg te duur. Het is belangrijk om hier op voorhand rekening mee te houden, zo voorkom je grote teleurstellingen. Hou in je achterhoofd dat jij en je partner het belangrijkst zijn op deze dag. Iedereen die hier extra bij aanwezig is, is alleen maar een plus. Een kleinere lijst van genodigden hoeft bovendien niet altijd een minpunt te zijn. Koppels die liever een intiem en kleinschalig huwelijk willen zullen dit net perfect vinden.
          </div>

          <div className='quote'>
            <p>" Locals weten als geen ander wie het lekkerste eten van de stad maakt en misschien zelfs welke lokale band iedereen op de dansvloer krijgt. " </p>
          </div>

          <div className='text'>
            <div className='subtitle'>Lokale pracht</div>
            Het is altijd een goed idee om voor je huwelijk al enkele keren de bestemming te bezoeken. Je ziet de mogelijkheden van de locatie, je kan persoonlijk afspreken met leveranciers en je kan te rade gaan bij locals. Vooral dat laatste kan heel wat voordelen bieden. Locals weten als geen ander wie het lekkerste eten van de stad maakt, wie de mooiste boeketten schikt en misschien zelfs welke lokale band iedereen op de dansvloer krijgt. Een extra voordeel van lokale leveranciers is dat ze vaak goedkoper zijn en dus flink de kosten kunnen drukken. De authentieke sfeer van het land krijg je er gratis bij.
          </div>

          <div className='text'>
            <div className='subtitle'>Rock your wedding</div>
            Lijkt dit je allemaal bergen werk? Het klopt dat trouwen in het buitenland iets complexer is en meer inspanning vraagt. Net daarom kan het interessant zijn om een beroep te doen op Rock my Wedding. Als weddingplanner hebben we ervaring met huwelijken in het buitenland en kunnen we heel wat last van de bruid en bruidegoms schouders nemen. Je hoeft bijvoorbeeld niet zelf op zoek te gaan naar leveranciers, wat je vaak al een tripje uitspaart. Het geld dat je daarmee bespaart kan je dan weer gebruiken voor leuke zaken zoals je huwelijksreis. Rock my Wedding helpt je niet alleen in het buitenland, ook in België zorgen we ervoor dat alles vlot verloopt. Van het uitnodigen van de gasten, het vinden van de perfecte locatie tot het regelen van vliegtuigtickets. Wij regelen alles in samenspraak met jullie zodat je zorgeloos kan uitkijken naar deze bijzondere dag.
            <br /><br /><br />
            Wil je meer informatie? Neem vrijblijvend contact op met <a href='http://rockmywedding.be/'>Rock my Wedding</a> en dan kijken we samen wat we voor jullie kunnen betekenen.
          </div>

          <div className='image-group'>
            <img src={b1} className='image-last'/>
            <img src={b3} className='image-last'/>
            <img src={b2} className='image-last'/>
          </div>
          
          <div className='socials'>
            <a class="resp-sharing-button__link" href="#" aria-label="">
              <div class="resp-sharing-button resp-sharing-button--facebook resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M18.77 7.46H14.5v-1.9c0-.9.6-1.1 1-1.1h3V.5h-4.33C10.24.5 9.5 3.44 9.5 5.32v2.15h-3v4h3v12h5v-12h3.85l.42-4z" /></svg>
              </div>
              </div>
            </a>

            <a class="resp-sharing-button__link" href="#" aria-label="">
              <div class="resp-sharing-button resp-sharing-button--twitter resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M23.44 4.83c-.8.37-1.5.38-2.22.02.93-.56.98-.96 1.32-2.02-.88.52-1.86.9-2.9 1.1-.82-.88-2-1.43-3.3-1.43-2.5 0-4.55 2.04-4.55 4.54 0 .36.03.7.1 1.04-3.77-.2-7.12-2-9.36-4.75-.4.67-.6 1.45-.6 2.3 0 1.56.8 2.95 2 3.77-.74-.03-1.44-.23-2.05-.57v.06c0 2.2 1.56 4.03 3.64 4.44-.67.2-1.37.2-2.06.08.58 1.8 2.26 3.12 4.25 3.16C5.78 18.1 3.37 18.74 1 18.46c2 1.3 4.4 2.04 6.97 2.04 8.35 0 12.92-6.92 12.92-12.93 0-.2 0-.4-.02-.6.9-.63 1.96-1.22 2.56-2.14z" /></svg>
              </div>
              </div>
            </a>

            <a class="resp-sharing-button__link" href="#" aria-label="">
              <div class="resp-sharing-button resp-sharing-button--pinterest resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M12.14.5C5.86.5 2.7 5 2.7 8.75c0 2.27.86 4.3 2.7 5.05.3.12.57 0 .66-.33l.27-1.06c.1-.32.06-.44-.2-.73-.52-.62-.86-1.44-.86-2.6 0-3.33 2.5-6.32 6.5-6.32 3.55 0 5.5 2.17 5.5 5.07 0 3.8-1.7 7.02-4.2 7.02-1.37 0-2.4-1.14-2.07-2.54.4-1.68 1.16-3.48 1.16-4.7 0-1.07-.58-1.98-1.78-1.98-1.4 0-2.55 1.47-2.55 3.42 0 1.25.43 2.1.43 2.1l-1.7 7.2c-.5 2.13-.08 4.75-.04 5 .02.17.22.2.3.1.14-.18 1.82-2.26 2.4-4.33.16-.58.93-3.63.93-3.63.45.88 1.8 1.65 3.22 1.65 4.25 0 7.13-3.87 7.13-9.05C20.5 4.15 17.18.5 12.14.5z" /></svg>
              </div>
              </div>
            </a>

            <a class="resp-sharing-button__link" href="#" aria-label="">
              <div class="resp-sharing-button resp-sharing-button--linkedin resp-sharing-button--small"><div aria-hidden="true" class="resp-sharing-button__icon resp-sharing-button__icon--solid">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path d="M6.5 21.5h-5v-13h5v13zM4 6.5C2.5 6.5 1.5 5.3 1.5 4s1-2.4 2.5-2.4c1.6 0 2.5 1 2.6 2.5 0 1.4-1 2.5-2.6 2.5zm11.5 6c-1 0-2 1-2 2v7h-5v-13h5V10s1.6-1.5 4-1.5c3 0 5 2.2 5 6.3v6.7h-5v-7c0-1-1-2-2-2z" /></svg>
              </div>
              </div>
            </a>

          </div>
        </div>
      </div>
    );
  }
}

export default App;
